#include "memory.h"
#include <immintrin.h>
#include <stdint.h>
#include <string.h>

uintptr_t align_down_uintptr(uintptr_t data, size_t align);
void *align_down(void *data, size_t align);
uintptr_t align_up_uintptr(uintptr_t data, size_t align);
void *align_up(void *data, size_t align);
void *match_align(void *dest, void *src, size_t align);
void fast_copy(void *dest, void *src, size_t n);

#pragma GCC push_options
#pragma GCC target("avx")
void avx_copy(void *dest, const void *src, size_t n) {
  /* Split into head, body, and tail by aligning.  The buffers must have the
   * same relative alignment though, if not the AVX copy will fault below due
   * to misalignment. */
  uintptr_t dest_addr = (uintptr_t)dest;
  uintptr_t dest_body_addr = align_up_uintptr(dest_addr, AVX_COPY_ALIGN);
  uintptr_t dest_body_end_addr = align_down_uintptr(dest_addr + n, AVX_COPY_ALIGN);
  size_t head_bytes = dest_body_addr - dest_addr;
  size_t tail_bytes = dest_addr + n - dest_body_end_addr;

  memcpy(dest, src, head_bytes);

  const __m256i *src_vec = (const __m256i*)((uintptr_t)src + head_bytes);
  __m256i *dest_vec = (__m256i*)(dest_body_addr);
  size_t vec_count = (dest_body_end_addr - dest_body_addr) / AVX_COPY_ALIGN;
  while(vec_count > 0) {
    const __m256i loaded = _mm256_load_si256(src_vec);
    _mm256_store_si256(dest_vec, loaded);

    --vec_count;
    ++src_vec;
    ++dest_vec;
  }
  _mm_sfence();

  memcpy((void*)dest_body_end_addr, (void*)((uintptr_t)src + n - tail_bytes),
    tail_bytes);
}

#pragma GCC pop_options
