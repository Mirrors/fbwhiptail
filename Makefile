
CFLAGS  ?= -g -O0
LDFLAGS ?=

all : fbwhiptail gtkwhiptail

test-menu-gtk: test-menu-gtk.c cairo_menu.c cairo_utils.c fbwhiptail_menu.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) \
		`pkg-config --cflags --libs cairo`                \
		`pkg-config --cflags --libs gtk+-2.0` -lm

test-menu-fb: test-menu-fb.c cairo_menu.c cairo_utils.c cairo_linuxfb.c \
		libcairo.a libpixman-1.a libpng16.a libz.a
	$(CC) $(CFLAGS) -o $@ $^ -lm $(LDFLAGS)

fbwhiptail: fbwhiptail.c fbwhiptail_menu.c cairo_menu.c cairo_utils.c \
	cairo_dri.c cairo_linuxfb.c cairo_hires_buffer.c memory.c shadowfb.c
	$(CC) $(CFLAGS) -o $@ $^ -lm -lcairo -lpixman-1 -lpng16 -lz $(LDFLAGS)

gtkwhiptail: fbwhiptail.c fbwhiptail_menu.c fbwhiptail_menu.h cairo_menu.c cairo_utils.c
	$(CC) $(CFLAGS) -DGTKWHIPTAIL -o $@ $^ -lm $(LDFLAGS)		\
		`pkg-config --cflags --libs cairo`              \
		`pkg-config --cflags --libs gtk+-2.0`

test-dri: test-dri.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm -f fbwhiptail test-menu-fb test-menu-gtk gtkwhiptail test-dri
