/* SPDX-License-Identifier: GPL-3.0-only */

#include "cairo_hires_buffer.h"
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

cairo_hires_buffer_t *cairo_hires_buffer_create(cairo_format_t format,
  int width, int height, double scale)
{
  cairo_hires_buffer_t *hrbuf = calloc(1, sizeof(cairo_hires_buffer_t));
  int physwidth, physheight;

  if (!hrbuf) {
    perror ("Error: can't allocate structure");
    return NULL;
  }

  physwidth = (int)ceil(width * scale);
  physheight = (int)ceil(height * scale);
  hrbuf->surface = cairo_image_surface_create (format, physwidth, physheight);
  hrbuf->cr = cairo_create (hrbuf->surface);
  cairo_scale (hrbuf->cr, scale, scale);
  hrbuf->logw = width;
  hrbuf->logh = height;
  hrbuf->scale = scale;

  return hrbuf;
}

void cairo_hires_buffer_destroy(cairo_hires_buffer_t *hrbuf)
{
  if (!hrbuf)
    return;

  cairo_destroy (hrbuf->cr);
  cairo_surface_destroy (hrbuf->surface);
  free(hrbuf);
}

cairo_t *cairo_hires_buffer_cairo(cairo_hires_buffer_t *hrbuf)
{
  return hrbuf ? hrbuf->cr : NULL;
}

cairo_surface_t *cairo_hires_buffer_surface(cairo_hires_buffer_t *hrbuf)
{
  return hrbuf ? hrbuf->surface : NULL;
}

int cairo_hires_buffer_logical_width(cairo_hires_buffer_t *hrbuf)
{
  return hrbuf ? hrbuf->logw : 0;
}

int cairo_hires_buffer_logical_height(cairo_hires_buffer_t *hrbuf)
{
  return hrbuf ? hrbuf->logh : 0;
}

double cairo_hires_buffer_scale(cairo_hires_buffer_t *hrbuf)
{
  return hrbuf ? hrbuf->scale : 0.0;
}

cairo_pattern_t *cairo_hires_buffer_pattern(cairo_hires_buffer_t *hrbuf,
  int target_x, int target_y)
{
  cairo_matrix_t pattern_transform;
  cairo_pattern_t *pattern;

  if (!hrbuf)
    return NULL;

  /* Scaling to the target space is done in the pattern transformation.
   * Conveniently, the pattern transform maps from user space to pattern space,
   * so we don't have to invert the scale.
   */
  cairo_matrix_init_scale (&pattern_transform, hrbuf->scale, hrbuf->scale);
  cairo_matrix_translate (&pattern_transform, -target_x, -target_y);

  pattern = cairo_pattern_create_for_surface (hrbuf->surface);
  cairo_pattern_set_matrix (pattern, &pattern_transform);

  return pattern;
}

void cairo_hires_buffer_paint(cairo_hires_buffer_t *hrbuf, cairo_t *target_cr,
  int target_x, int target_y)
{
  cairo_pattern_t *source_pattern;

  if (!hrbuf || !target_cr)
    return;

  source_pattern = cairo_hires_buffer_pattern (hrbuf, target_x, target_y);

  cairo_save (target_cr);

  cairo_set_source (target_cr, source_pattern);
  cairo_pattern_destroy (source_pattern);
  cairo_paint (target_cr);

  cairo_restore (target_cr);
}

void cairo_hires_buffer_mask(cairo_hires_buffer_t *hrbuf, cairo_t *target_cr,
  int target_x, int target_y)
{
  cairo_pattern_t *mask_pattern;

  if (!hrbuf || !target_cr)
    return;

  mask_pattern = cairo_hires_buffer_pattern (hrbuf, target_x, target_y);

  cairo_mask (target_cr, mask_pattern);
  cairo_pattern_destroy (mask_pattern);
}
