#include "shadowfb.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>

struct shadowfb {
  /* Actual video memory buffer.  It is best to only access this in a single
   * streaming write pass, it may be across a PCIe link where random access is
   * slow.  This is set before creating shadowfb.
   */
  uint8_t *fb_data;
  /* Size of the actual framebuffer */
  uint32_t fb_size;
  /* Shadow buffer raw allocation, shadow_fb_data is aligned to match fb_data
   * but we must use the original address to free */
  uint8_t *shadow_fb_raw;
};

struct shadowfb *shadowfb_create(uint8_t *fb_data, uint32_t fb_size)
{
  if (!fb_data || !fb_size) {
    fprintf(stderr, "Invalid framebuffer");
    return NULL;
  }

  struct shadowfb *sfb = malloc(sizeof(struct shadowfb));
  if (sfb == NULL) {
    perror("Failed to allocate struct shadowfb");
    return NULL;
  }

  sfb->shadow_fb_raw = malloc(fb_size + AVX_COPY_ALIGN);
  if (sfb->shadow_fb_raw == NULL) {
    free(sfb);
    perror("Failed to allocate shadow framebuffer");
    return NULL;
  }

  sfb->fb_data = fb_data;
  sfb->fb_size = fb_size;
  return sfb;
}

uint8_t *shadowfb_destroy(struct shadowfb *sfb, uint32_t *fb_size)
{
  if (!sfb)
    return NULL;

  free(sfb->shadow_fb_raw);
  if (fb_size)
    *fb_size = sfb->fb_size;

  uint8_t *fb_data = sfb->fb_data;
  free(sfb);
  return fb_data;
}

uint8_t *shadowfb_get_draw_base(struct shadowfb *sfb)
{
  /* Match the relative alignment of fb_data to allow AVX fast copy */
  return match_align(sfb->shadow_fb_raw, sfb->fb_data, AVX_COPY_ALIGN);
}

void shadowfb_present(struct shadowfb *sfb)
{
  if (!sfb)
    return;

  fast_copy(sfb->fb_data, shadowfb_get_draw_base(sfb), sfb->fb_size);
}

void shadowfb_capture(struct shadowfb *sfb)
{
  if (!sfb)
    return;

  fast_copy(shadowfb_get_draw_base(sfb), sfb->fb_data, sfb->fb_size);
}
