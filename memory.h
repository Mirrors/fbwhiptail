#ifndef __MEMORY_H__
#define __MEMORY_H__

#include <stdint.h>
#include <string.h>

/* Adjust a pointer to a given alignment by decreasting the pointer.  align must
 * be a power of 2.  May subtract up to 'align-1', so there must be space
 * available.
 */
inline uintptr_t align_down_uintptr(uintptr_t data, size_t align)
{
  return data & ~(align-1);
}
inline void *align_down(void *data, size_t align)
{
  return (void*)align_down_uintptr((uintptr_t)data, align);
}
/* Adjust a pointer to a given alignment by increasing the pointer.  align must
 * be a power of 2.  May add up to 'align-1' to the pointer, so overallocate.
 */
inline uintptr_t align_up_uintptr(uintptr_t data, size_t align)
{
  return align_down_uintptr(data + align - 1, align);
}
inline void *align_up(void *data, size_t align)
{
  return (void*)align_up_uintptr((uintptr_t)data, align);
}

/* Match the alignment of a destination buffer to a source buffer - allows
 * fast_copy() to be used with a destination buffer when the source buffer's
 * alignment is not controlled.  Aligns up by up to 'align-1', so overallocate
 * the destination buffer.
 */
inline void *match_align(void *dest, void *src, size_t align)
{
  uintptr_t align_mask = align - 1;
  uintptr_t align_offset = align + ((uintptr_t)dest & align_mask) - ((uintptr_t)src & align_mask);
  align_offset &= align_mask;
  return (void*)((uintptr_t)dest + align_offset);
}

/* Copy data fast with AVX.  (Does not test for availability of AVX, caller
 * must test).  This results in a major speedup for copying a framebuffer to
 * Aspeed VRAM in particular.
 *
 * This is not fully compatible with memcpy - the source and destination must
 * have the same 32-byte-relative alignment.  (They don't necessarily have to
 * be 32-byte aligned, head and tail are handled with memcpy, but the relative
 * alignment must be the same.)
 *
 * This function is compiled with -mavx to access AVX intrinsics, so use
 * fast_copy() to check for support and fall back to memcopy if needed.
 */
#define AVX_COPY_ALIGN 32
void avx_copy(void *dest, const void *src, size_t n);

/* Copy with avx_copy() if available, or memcpy() otherwise.  The buffers must
 * meet the alignment requirements of avx_copy(), this falls back if the CPU
 * does not support AVX.
 */
inline void fast_copy(void *dest, void *src, size_t n)
{
  __builtin_cpu_supports("avx") ? avx_copy(dest, src, n) : memcpy(dest, src, n);
}

#endif
