#ifndef __SHADOWFB_H__
#define __SHADOWFB_H__

#include <stdint.h>

/* shadowfb - Creates a shadowframebuffer for direct drawing in system memory,
 * then bulk-copying to video memory
 */

/* Create a shadowfb.  Provide the real framebuffer base address and size.
 * Returns NULL for error.
 */
struct shadowfb *shadowfb_create(uint8_t *fb_data, uint32_t fb_size);

/* Destroy a shadowfb.  This frees the shadow buffer.  The real framebuffer is
 * returned so it can be destroyed if needed.  Return value is the framebuffer
 * base address, size is stored in *fb_size if given.
 */
uint8_t *shadowfb_destroy(struct shadowfb *sfb, uint32_t *fb_size);

/* Get the shadow framebuffer base address where drawing begins.  (Note that
 * this differs from shadow_fb_raw due to alignment.)
 */
uint8_t *shadowfb_get_draw_base(struct shadowfb *sfb);

/* Present the shadow framebuffer by copying to the real framebuffer. */
void shadowfb_present(struct shadowfb *sfb);

/* Capture the real frame buffer to the shadow buffer (reverse of
 * ..._present()).  Used to back up and restore the console in the LinuxFB
 * backend.
 */
void shadowfb_capture(struct shadowfb *sfb);

#endif
