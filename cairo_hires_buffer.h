/* SPDX-License-Identifier: GPL-3.0-only */

#ifndef __CAIRO_HIRES_BUFFER_H__
#define __CAIRO_HIRES_BUFFER_H__

#include <cairo/cairo.h>

/* cairo_hires_buffer() is an offscreen Cairo drawing surface with an associated
 * high-resolution scale.  Its logical coordinates are scaled to the surface
 * pixels, and it can be painted into another buffer at the proper scale.
 */
typedef struct
{
  cairo_surface_t *surface;
  cairo_t *cr;
  int logw, logh;
  double scale;
} cairo_hires_buffer_t;

/* Create a cairo_hires_buffer with the desired logical size and scale. */
cairo_hires_buffer_t *cairo_hires_buffer_create(cairo_format_t format,
  int width, int height, double scale);
/* Destroy a cairo_hires_buffer */
void cairo_hires_buffer_destroy(cairo_hires_buffer_t *hrbuf);

/* Get the cairo drawing context for drawing into the buffer */
cairo_t *cairo_hires_buffer_cairo(cairo_hires_buffer_t *hrbuf);

/* Get the surface within the buffer.  You can ref the surface and destroy the
 * hires buffer to extract the surface for manual drawing.
 */
cairo_surface_t *cairo_hires_buffer_surface(cairo_hires_buffer_t *hrbuf);

/* Get the logical width/height of the buffer (the original desired size,
 * unaffected by any rounding).
 */
int cairo_hires_buffer_logical_width(cairo_hires_buffer_t *hrbuf);
int cairo_hires_buffer_logical_height(cairo_hires_buffer_t *hrbuf);

/* Get the high-resolution scale of the buffer */
double cairo_hires_buffer_scale(cairo_hires_buffer_t *hrbuf);

/* Create a pattern to use this surface in another context at the given position
 * (in the target context's logical coordinates).  The caller owns the returned
 * pattern and must destroy it.  This is used to implement the paint and mask
 * operations.
 */
cairo_pattern_t *cairo_hires_buffer_pattern(cairo_hires_buffer_t *hrbuf,
  int target_x, int target_y);

/* Draw the buffer into another drawing context at the specified position (in
 * the target context's logical coordinates)
 */
void cairo_hires_buffer_paint(cairo_hires_buffer_t *hrbuf, cairo_t *target_cr,
  int target_x, int target_y);

/* Draw using the current source pattern masked by this buffer's alpha
 * channel
 */
void cairo_hires_buffer_mask(cairo_hires_buffer_t *hrbuf, cairo_t *target_cr,
  int target_x, int target_y);

#endif
